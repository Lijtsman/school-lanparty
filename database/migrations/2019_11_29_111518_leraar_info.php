<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class LeraarInfo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('leraar_info', function (Blueprint $table) {
            $table->smallIncrements('leraar_id');
            $table->string('leraar_naam', 32);
            $table->string('waar', 64);
            $table->boolean('ma');
            $table->boolean('di');
            $table->boolean('wo');
            $table->boolean('do');
            $table->boolean('vr');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
