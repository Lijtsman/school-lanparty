<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class SlideImages extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('slide_images', function (Blueprint $table) {
            $table->smallIncrements('image_id');
            $table->string('slide_image', 512);
            $table->string('img_name', 64)->nullable();
            $table->string('img_desc', 128)->nullable();
        });

        DB::table('slide_images')->insert(['image_id' => NULL, 'slide_image' => "https://3mxwfc45nzaf2hj9w92hd04y-wpengine.netdna-ssl.com/wp-content/uploads/2015/03/inside-page-style-blank-3.jpg", 'img_name' => 'sample text', 'img_desc' => 'sample text 2']);

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
