<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('lame');
            $table->string('email')->unique();
            $table->char('user_qr')->nullable();
            $table->boolean('gekocht')->default(0);
            $table->timestamps();
        });
        DB::table('users')->insert(['id' => NULL, 'name' => "admin", 'lame' => "admin", 'email' => 'admin', 'gekocht' => '1']);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
