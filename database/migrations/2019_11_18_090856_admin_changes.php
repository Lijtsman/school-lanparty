<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class AdminChanges extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admin_changes', function (Blueprint $table) {
            $table->char('lanparty_datum', 23)->nullable();
            $table->char('textTitel', 64)->nullable();
            $table->char('textTLDR', 255)->nullable();
            $table->text('textText')->nullable();
            $table->text('rState')->nullable();
        });

        DB::table('admin_changes')->insert(['lanparty_datum' => "NULL", 'textTitel' => "lorem ipsum", 'textText' => "dolor sit amet", 'textTLDR' => 'poyo', 'rState' => '1']);

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
