@extends('layouts.app')

<?php
use Illuminate\Support\Facades\DB;

//haal data uit de database
$admindingen = DB::table('admin_changes')->get();

//haal de countdown datum uit $admindingen
foreach ($admindingen as $adminding) {
    ($datum = $adminding->lanparty_datum);
}

//Haal slide_images uit de database
$adminPlaatjes = DB::table('slide_images')->get();

//als er geen images zijn zet een default image
if (count($adminPlaatjes) == 0) {
    DB::table('slide_images')->insert(['image_id' => NULL, 'slide_image' => "https://3mxwfc45nzaf2hj9w92hd04y-wpengine.netdna-ssl.com/wp-content/uploads/2015/03/inside-page-style-blank-3.jpg", 'img_name' => 'sample text', 'img_desc' => 'sample text 2']);
    echo "<script>location.reload();</script>";
} //als de default image de eerste value uit de database is en er komen daarna andere verwijder dan het plaatje en herlaad de pagina
elseif (count($adminPlaatjes) > 1 and $adminPlaatjes[0]->slide_image == "https://3mxwfc45nzaf2hj9w92hd04y-wpengine.netdna-ssl.com/wp-content/uploads/2015/03/inside-page-style-blank-3.jpg") {
    DB::table('slide_images')->where("slide_image", "=", "https://3mxwfc45nzaf2hj9w92hd04y-wpengine.netdna-ssl.com/wp-content/uploads/2015/03/inside-page-style-blank-3.jpg")->delete();
    echo "<script>location.reload();</script>";
}
try {
    //check of een datum gezet is en zet een countdown neer
    if ($datum !== NULL) {
        echo '<label id="deDatum" class="' . $datum . '"></label><script>var countDownDate = new Date(document.getElementById("deDatum").className).getTime();var x = setInterval(function() {var now = new Date().getTime();var distance = countDownDate - now;var days = Math.floor(distance / (1000 * 60 * 60 * 24));var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));var seconds = Math.floor((distance % (1000 * 60)) / 1000);document.getElementById("timer").innerHTML = days + "d " + hours + "h "+ minutes + "m " + seconds + "s ";if (distance < 0) {clearInterval(x);document.getElementById("timer").innerHTML = "De lanparty is nu";}}, 1000);</script>';
    }
} catch (Exception $e) {
    DB::table('admin_changes')->insert(['lanparty_datum' => NULL, 'textTitel' => "lorem ipsum", 'textText' => "dolor sit amet", 'textTLDR' => 'poyo']);
}
?>

@section('content')
    <body>
    <div style="padding-top: 2%;" class="container">
        <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
            <ol style="background-color:rgba(0,0,0,.2);" class="carousel-indicators">
                <?php
                $first = true;
                foreach ($adminPlaatjes as $adminPlaatje) {
                    if ($first = true) {
                        echo '<li data-target="#carouselExampleIndicators" data-slide-to=' . $adminPlaatje->image_id . ' class="active"></li>';
                        $first = false;
                    } else {
                        echo '<li data-target="#carouselExampleIndicators" data-slide-to=' . $adminPlaatje->image_id . '></li>';
                    }
                }
                ?>
            </ol>
            <div class="carousel-inner">
                <?php
                $first = true;
                foreach ($adminPlaatjes as $adminPlaatje) {
                    if ($first == true) {
                        echo '<div class="carousel-item active"><img height="600px" width="640px" class="d-block w-100" src=' . $adminPlaatje->slide_image . ' alt="First slide">';
                        $first = false;
                        //Zet de naam en beschrijving van het plaatje als die beschikbaar zijn
                        if (isset($adminPlaatje->img_desc) or isset($adminPlaatje->img_name)) {
                            echo '<div class="carousel-caption d-none d-md-block" style="background-color:rgba(0,0,0,.3);">';
                            if (isset($adminPlaatje->img_name)) {
                                echo '<h5>' . $adminPlaatje->img_name . '</h5>';
                            }
                            if (isset($adminPlaatje->img_desc)) {
                                echo '<p> ' . $adminPlaatje->img_desc . '</p>';
                            }
                            echo '</div>';
                        }
                        echo '</div>';
                    } else {
                        echo '<div class="carousel-item" ><img height = "600px" width = "640px" class="d-block w-100" src = ' . $adminPlaatje->slide_image . ' alt = "not the first slide" >';
                        if (isset($adminPlaatje->img_desc) or isset($adminPlaatje->img_name)) {
                            echo '<div class="carousel-caption d-none d-md-block" style="background-color:rgba(0,0,0,.3);">';
                            if (isset($adminPlaatje->img_name)) {
                                echo '<h5>' . $adminPlaatje->img_name . '</h5>';
                            }
                            if (isset($adminPlaatje->img_desc)) {
                                echo '<p> ' . $adminPlaatje->img_desc . '</p>';
                            }
                            echo '</div>';
                        }
                        echo '</div>';
                    }
                }
                ?>
            </div>
            <a style="width: 50%;" class="carousel-control-prev"
               href="#carouselExampleIndicators" role="button" data-slide="prev">
                <span class="sr-only">Previous</span>
            </a>
            <a style="width: 50%;" class="carousel-control-next"
               href="#carouselExampleIndicators" role="button" data-slide="next">
                <span class="sr-only">Next</span>
            </a>
        </div>
    </div>
    <br>
    <div class="container" style=" text-align: center; ">
        <div class="container" style="width: 900px;">
            <div class="row justify-content-center">
                <p id="timer" style="color: black;
                    font-style: italic;
                    font: 72px 'Orbitron', sans-serif;

                       "></p>
            </div>
        </div>
        <?php
        try {
            echo '<h3>' . $datum . ' </h3>';
        } catch (exception $e) {
            echo "Er is nog geen datum gesteld";
        }
        ?>
        <form action="/register">
            {{csrf_field()}}
            <button type="submit" style="background-color: #f5fcfc; color:black" class="btn btn-lg">
                Schrijf je nu in!
            </button>
        </form>
    </div>
    </body>
    <script>
        $(' . carousel').carousel()
    </script>
@endsection



