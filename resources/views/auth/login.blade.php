@extends('layouts.app')

@section('content')
    <br>
    <body style="
   color:black;
   background: url('https://media0.giphy.com/media/fVsVfxVwz40I24GT7X/giphy.gif?cid=790b761187153b23c5e3972564b2be1696dee333ac7cdd38&rid=giphy.gif')
   no-repeat center center fixed;
  -webkit-background-size: cover;
  -moz-background-size: cover;
  -o-background-size: cover;
  background-size: cover;

">
    <div class="container" style="margin-top: 7%">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">
{{--                        style="background-image: url('https://media0.giphy.com/media/26xBSZvEq1ZK2kWaI/giphy.gif?cid=790b761105f449af5fe89bd195f34c6a1a67f548d11433a0&rid=giphy.gif');--}}
{{--                        background-size: 100% 160%;--}}
                         {{ __('Ｌｏｇｉｎ　ヨ汚な') }}
                    </div>
                    <div class="card-body" style="
                    /*background-image: url('https://media0.giphy.com/media/9J5bKYitolcHuER9rl/giphy.gif?cid=790b76116dcfa2a4dca586f81b3d76ac91dc82ecbdd56c36&rid=giphy.gif');*/
                    /* background-size: 100% 150% ;*/
">
                        <form method="POST" action="{{ route('login') }}">
                            @csrf

                            <div class="form-group row">
                                <label for="email"
                                       class="col-md-4 col-form-label text-md-right">{{ __('【﻿Ｅｍａｉｌ】') }}</label>

                                <div class="col-md-6">
                                    <input id="email" type="email"
                                           class="form-control @error('email') is-invalid @enderror"
                                           name="email" value="{{ old('email') }}" required autocomplete="email"
                                           autofocus>

                                    @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="password"
                                       class="col-md-4 col-form-label text-md-right">{{ __('【﻿Ｐａｓｓｗｏｒｄ】') }}</label>

                                <div class="col-md-6">
                                    <input id="password" type="password"
                                           class="form-control @error('password') is-invalid @enderror" name="password"
                                           required autocomplete="current-password">

                                    @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-md-6 offset-md-4">
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox" name="remember"
                                               id="remember" {{ old('remember') ? 'checked' : '' }}>

                                        <label class="form-check-label" for="remember">
                                            {{ __('【﻿Ｒｅｍｅｍｂｅｒ　ｍｅ】') }}
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row mb-0">
                                <div class="col-md-8 offset-md-4">
                                    <button type="submit"
                                            style=" background-image: url('https://media1.giphy.com/media/dvreHY4p06lzVSDrvj/giphy.gif?cid=790b7611641ddee7882d4350fff5ca1d764f85bb70a6a8e5&rid=giphy.gif')"
                                            class="btn btn-primary">
                                        {{ __('Ｌｏｇｉｎ　ヨ汚な') }}
                                    </button>

                                    @if (Route::has('password.request'))
                                        <a class="btn btn-link" style="color: white"
                                           href="{{ route('password.request') }}">
                                            {{ __('Ｆｏｒｇｏｔ　ｙｏｕｒ　ｐａｓｓｗｏｒｄ？　鉛ち線') }}
                                        </a>
                                    @endif
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </body>

@endsection
