@extends('layouts.app')

@section('content')
    <br>

    {{--    <body style="background-color: #9f9f9;">--}}
    <body style="color: #f9f9f9;">
    <div class="container" style="margin-top: 7%">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header" style="background-color:white">
                        {{ __('Aanmeldings Formulier') }}
                    </div>
                    <form method="POST" action="{{ route('register') }}">
                        @csrf
                        <div class="form-group row" style="padding-top: 3%">
                            <label for="name"
                                   class="col-md-4 col-form-label text-md-right">{{ __('Naam') }}</label>

                            <div class="col-md-6">
                                <input id="name" type="text"
                                       class="form-control @error('name') is-invalid @enderror" name="name"
                                       value="{{ old('name') }}" required autocomplete="name" autofocus>

                                @error('name')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="lame"
                                   class="col-md-4 col-form-label text-md-right">{{ __('Achternaam') }}</label>

                            <div class="col-md-6">
                                <input id="lame" type="text"
                                       class="form-control @error('name') is-invalid @enderror" name="lame"
                                       value="{{ old('lame') }}" required autocomplete="lame" autofocus>

                                @error('name')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="email"
                                   class="col-md-4 col-form-label text-md-right">{{ __('e-Mail') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email"
                                       class="form-control @error('email') is-invalid @enderror" name="email"
                                       value="{{ old('email') }}" required autocomplete="email">

                                @error('email')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="snack"
                                   class="col-md-4 col-form-label text-md-right">{{ __('Snack') }}</label>
                            <div class="col-md-6">
                                <label>
                                    <select class="browser-default custom-select" name="snack">
                                        <option selected>Niks</option>
                                        <option value="frikandel">frikandel</option>
                                        <option value="kroket">kroket</option>
                                        <option value="Kaassouffle">kaassouffle</option>
                                    </select>
                                </label>
                            </div>
                        </div>


                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btnextra"
                                        style="margin-bottom: 3%; color:black;background-color: #f5fcfc">
                                    {{ __('Schrijf je in!') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    </body>
@endsection
