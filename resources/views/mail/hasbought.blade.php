<html>
<head>
    <?php
    use Illuminate\Support\Facades\Auth;use SimpleSoftwareIO\QrCode\Facades\QrCode;
    ?>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
<h1>Beste {{Auth::user()->name}} {{Auth::user()->lame}}</h1><br>

Je registratie is bevestigt. Je Ticketnummer is {{Auth::user()->id}}<br>

Je digitale ticket:<br>

<?php
QrCode::format('png')->size(500)->generate(Auth::user()->user_qr, '../public/images/qrcodes/qrcode' . Auth::user()->id . '.png');
$image = '../public/images/qrcodes/qrcode' . Auth::user()->id . '.png';
?>
<img src="{{ $message->embed($image) }}">

<br>
<br>

Deze qr-code gebruik je om te betalen* en uiteindelijk als betaalbewijs op de Lanparty zelf, Dus bewaar deze
mail
goed.<br>

<br>
* betalen moet op school voor meer informatie zie www.landstedelanparty.nl/help<br>
Tot dan,<br>
Landstede
</body>
</html>

