@extends('layouts.app')

@section('content')

    <div style="padding-top: 10%; text-align: center">
        <form method="POST" action="/adminPanel">@csrf
            <input type="submit" name="adminPanel" value="Ga naar de admin panel">
        </form>
        <br>
        <form method="POST" action="/qrscanner">@csrf
            <input type="submit" name="qrScanner" value="Ga naar de qr scanner">
        </form>
        <br>
        <form method="POST" action="/admininfo">@csrf
            <input type="submit" name="adminLeraar" value="Ga naar de leraren pagina">
        </form>
    </div>
@stop

