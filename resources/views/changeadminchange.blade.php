@extends("layouts.app")
<link href="http://netdna.bootstrapcdn.com/twitter-bootstrap/2.2.2/css/bootstrap-combined.min.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" media="screen"
      href="http://tarruda.github.com/bootstrap-datetimepicker/assets/css/bootstrap-datetimepicker.min.css">
<script src="{{ asset('js/app.js') }}" defer></script>

@section('content')
    <?php
    session_start();
    use Illuminate\Support\Facades\DB;


    $slide_images = DB::table('slide_images')->get();
    $admin = new App\Http\Controllers\adminController;
    $admindingen = new App\Http\Controllers\databaseController;
    $admindingen = $admindingen->verkrijgAdmin();

    if (isset($_POST['changeDate'])) {
        $admin->changeDate($_POST['newDate']);
    }

    if (isset($_POST['textUpload'])) {
        $admin->textUpload(['textUpload']);
    }


    if (isset($_POST['addImg'])) {
        $admin->addImg($_POST["newImgLink"]);
    }

    if (isset($_POST['update'])) {

    }

    if (isset($_POST['delete'])) {

    }
    ?>


    <body style="color:darkgray;">
    <div class="container" style="padding-top: 10%;">
        <div class="row">
            <div style="float:left" class="col">
                <form method="post">@csrf
                    Klik hier om een datum te kiezen<br>
                    <div id="datetimepicker" class="input-append date">
                        <input name="newDate" style="height: auto" type="text">
                        <span style="height: inherit" class="add-on">
                  <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                </span>
                    </div>
                    <br>
                    <input type="submit" class="btn btn-primary" name="changeDate" placeholder="update"><br>
                    <small>*Tijd is optioneel</small>
                </form>
                <form method="post" onsubmit="">@csrf
                    <label>Afbeelding toevoegen</label>

                    <input type="text" class="form-control" name="newImgLink" placeholder="Afbeelding Link"><br>

                    <input onkeyup="countImgTitel(this.value)" type="text" class="form-control" name="img_name"
                           placeholder="Titel afbeelding (optioneel)">
                    <span id=timg>0/64</span><br>

                    <input onkeyup="countDesc(this.value)" type="text" class="form-control" name="img_desc"
                           placeholder="Afbeelding beschrijving (optioneel)">
                    <span id=dimg>0/128</span><br>

                    <input type="submit" class="btn btn-primary" name="addImg" placeholder="Toevoegen">

                </form>
                <form method="post">@csrf
                    <label>Selecteer plaatje</label>
                    <select name="whichImage" id="imagePick" onchange="changeText()">
                        <?php
                        foreach ($slide_images as $slide_image) {
                            echo "<option name='user_id' value='$slide_image->image_id'>$slide_image->slide_image</option>";
                        }
                        ?>
                    </select>
                    <input class="btn btn-danger " type="submit" name="delete" value="delete">
                </form>
                <br>
                <form method="get">@csrf
                    <?php try { ?>
                    <img height="126px" width="126px" id="showSamp" src="{{$slide_images[0]->slide_image}}"
                         alt="example"><br>
                    <?php } catch (Exception $e) {
                    } ?>
                    <label>Verander naar</label>
                    <br>
                    <input type="hidden" id="oldImg" value=" " name="oldImg ">
                    <input type="text" class="form-control" name="newImg">
                    <button type="submit" name="update" value="update" class="btn btn-primary">Submit</button>
                </form>
            </div>
            <div class="col" style="float:right">
                <form method="post" style="padding-bottom: 5%">@csrf
                    TLDR
                    <textarea class="form-control" rows="3" onkeyup="countTLDR(this.value)"
                              type="text"
                              name="textTLDR">{{$admindingen[0]->textTLDR}}</textarea>
                    <span id=TLDR>{{strlen($admindingen[0]->textTLDR)}}/255</span><br>

                    TITEL
                    <input class="form-control input-sm" onkeyup="countTitel(this.value)" type="text" name="textTitel"
                           value="{{$admindingen[0]->textTitel}}">
                    <span id=titel>{{strlen($admindingen[0]->textTitel)}}/64</span><br>
                    TEXT
                    <div style="padding-top:5px " class="form-group">
                <textarea id="textbox" style="white-space: pre-wrap" name="textText" class="form-control rounded-0"
                          placeholder="text van de info pagina"
                          rows="10"
                          onkeyup="charcountupdate(this.value)">{{$admindingen[0]->textText}}</textarea>

                        <span id=charcount>{{strlen($admindingen[0]->textText)}}/65.535</span>
                    </div>
                    <input type="submit" name="textUpload" value="upload text">
                </form>
            </div>
        </div>
    </div>
    </body>
    <script type="text/javascript"
            src="http://cdnjs.cloudflare.com/ajax/libs/jquery/1.8.3/jquery.min.js">
    </script>
    <script type="text/javascript"
            src="http://netdna.bootstrapcdn.com/twitter-bootstrap/2.2.2/js/bootstrap.min.js">
    </script>
    <script type="text/javascript"
            src="http://tarruda.github.com/bootstrap-datetimepicker/assets/js/bootstrap-datetimepicker.min.js">
    </script>
    <script type="text/javascript"
            src="http://tarruda.github.com/bootstrap-datetimepicker/assets/js/bootstrap-datetimepicker.pt-BR.js">
    </script>
    <script type="text/javascript">
        $('#datetimepicker').datetimepicker({
            format: 'MM/dd/yyyy hh:mm:ss',
        });
    </script>
    <script>
        function changeText() {
            let url = $('#imagePick option:selected').text();
            document.getElementById("showSamp").src = url;
            document.getElementById("oldImg").value = url;
        }

        function charcountupdate(str) {
            var lng = str.length;
            document.getElementById("charcount").innerHTML = lng + '/65.535';
        }

        function countTLDR(str) {
            var lng = str.length;
            document.getElementById("TLDR").innerHTML = lng + '/255';
        }

        function countTitel(str) {
            var lng = str.length;
            document.getElementById("titel").innerHTML = lng + '/64';
        }

        function countImgTitel(str) {
            var lng = str.length;
            document.getElementById("timg").innerHTML = lng + '/64';
        }

        function countDesc(str) {
            var lng = str.length;
            document.getElementById("dimg").innerHTML = lng + '/128';
        }
    </script>
@endsection()


