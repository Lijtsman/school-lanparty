<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>

    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <link href="https://fonts.googleapis.com/css?family=Orbitron&display=swap" rel="stylesheet">

    {{--VOORSTEL 7--}}
    <link href="{{asset('css/fade(voorstel7).css')}}" rel="stylesheet">


    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('title','Lanparty')</title>

    <!-- Scripts -->

@yield('qrScripts')
{{--    <script src="{{ asset('js/app.js') }}" defer></script>--}}


<!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Orbitron&display=swap" rel="stylesheet">


    <!-- Styles -->


</head>


{{--voorstel 6--}}
<div id="divA" class="block">
    <header>
        <ul class="ul" id="hoofd" style="padding-bottom: 1%; padding-top:1%;">
            <li class="li" style="padding-left: 5%">
                <a href="/">
                    <img src="https://www.detechniekacademie.nl/wp-content/uploads/2017/08/landstede-logo.png"
                         width="10%">
                </a>
            </li>
            <li class="li"><a class="lia" style="" href="/">Home</a></li>
            <li class="li"><a class="lia" href="/about">Info</a></li>
            @yield('adminHeader')
        </ul>
    </header>

</div>
<body class="desaus">
<div id='stars' style="position:fixed"></div>
<div id='stars2' style="position:fixed"></div>
<div id='stars3' style="position:fixed"></div>
@yield('content')
</body>
</html>


{{--voorstel 1-5--}}
{{--<div id="divA" class="block">--}}
{{--    <header>--}}
{{--        <nav--}}
{{--            id="hoofd"--}}
{{--            style=padding-bottom:15vh;@yield ("headerImg","background:url(https://i.ytimg.com/vi/1iIXx55Orwg/maxresdefault.jpg)")>--}}
{{--            <div style="padding-bottom: 1%;padding-top:1%;text-align: center">--}}
{{--                <li style="text-decoration: underline;color: whitesmoke; font-size: 24px; float:left"--}}
{{--                    ref="/">--}}
{{--                    <img--}}
{{--                        src="https://nineplanets.org/wp-content/uploads/2019/09/earth.png"--}}
{{--                        alt="planet" height="auto"--}}
{{--                        width="120vw">   Home--}}
{{--                </li>--}}
{{--                <li style="text-decoration: underline; color: whitesmoke; font-size: 24px; float: right"--}}
{{--                    ref="/about">Info   <img--}}
{{--                        src="https://carlisletheacarlisletheatre.org/images/moon-png-3.png"--}}
{{--                        alt="planet" height="auto"--}}
{{--                        width="60vw"></li>--}}
{{--            </div>--}}
{{--        </nav>--}}
{{--    </header>--}}
{{--</div>--}}
