@extends('layouts.app')

@section('script')
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/jquery-ui-timepicker-addon/1.6.3/jquery-ui-timepicker-addon.min.css"/>

    <script>
        jQuery(function ($) {
            $("#datepicker").datetimepicker();
        });
    </script>
@endsection

@section('content')
    <?php
    session_start();
    use App\Http\Controllers\databaseController;
    $temp = new databaseController();
    $leraren = $temp->verkrijgLeraren();
    if (isset($_POST['submit'])) {
        try {

//            DB::table('slide_images')->insert(['image_id' => NULL, 'slide_image' => $_POST['newImgLink'], 'img_name' => $_POST['img_name'], 'img_desc' => $_POST['img_desc']]);
        } catch (exception $e) {

        }
    }


    if (isset($_POST['delete'])) {
//        DB::table('slide_images')->where("image_id", $_POST['whichImage'])->delete();
    }
    ?>

    <body>
    <div style="padding-top: 10%; padding-left: 30%">
        <form method="post">{{csrf_field()}}
            <div class="row">
                <?php
                $dagen = array(
                    $dagen[0] = "ma",
                    $dagen[1] = "di",
                    $dagen[2] = "wo",
                    $dagen[3] = "do",
                    $dagen[4] = "vr",
                );
                ?>
                <div class="col"></div>
                <?php
                foreach ($dagen as $dag) {?>
                <div class="col">

                    <input type="text" readonly class="form-control" value="{{$dag}}">
                </div>
                <?php }?>
                <div class="col"></div>

            </div>
        </form>
        <form method="post">{{csrf_field()}}
            <br>
            <saus style="" id="copDit" class="row">

                <div class="col">
                    <input type="text" class="form-control" name="leraar_naam" placeholder="Naam leraar"><br>
                </div>
                <br>
                <?php
                foreach ($dagen as $dag){?>
                <div class="col">
                    <input type="checkbox" class="form-control" name="besch_op_{{$dag}}">
                </div>
                <?php }?>
                <div class="col">
                    <input type="submit" class="form-control" name="submit" value="submit">
                </div>

            </saus>

        </form>

        <?php
        foreach ($leraren as $leraar) {
        $lnaam = $leraar->leraar_naam;
        $benschop = array(
            $beschop[0] = $leraar->ma,
            $beschopd[1] = $leraar->di,
            $beschopw[2] = $leraar->wo,
            $beschop[3] = $leraar->do,
            $beschop[4] = $leraar->vr,
        );
        $id = $leraar->leraar_id;
        ?>

        <form method="post">{{csrf_field()}}
            <br>
            <saus style="" id="copDit" class="row">
                <div class="col">
                    <input type="text" class="form-control" name="leraar_naam" value="{{$lnaam}}"><br>
                </div>
                <br>
                <?php
                foreach ($benschop as $beschikbaar) {
                if (!isset($x)) {
                    $x = 0;
                }
                if ($beschikbaar == 1) {
                ?>
                <div class="col">
                    <input type="checkbox" checked class="form-control" name="beschikbaar_{{$x}}">
                </div>

                <?php
                } else {
                ?>
                <div class="col">
                    <input type="checkbox" class="form-control" name="beschikbaar_{{$x}}">
                </div>
                <?php
                $x++;
                }
                }
                ?>
                <div class="col">
                    <input type="submit" class="form-control" name="submit" value="submit">
                </div>

            </saus>
        </form>
        <?php
        }?>

    </div>
    <div style="padding-top: 10%">


    </div>
    </body>

@endsection()
