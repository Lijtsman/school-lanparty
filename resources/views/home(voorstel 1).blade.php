<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>


    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://fonts.googleapis.com/css?family=Orbitron&display=swap" rel="stylesheet">


    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('title','Lanparty')</title>

    <!-- Scripts -->

    <script src="{{ asset('js/app.js') }}" defer></script>


    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{asset('css/app.css')}}" rel="stylesheet">
    <link href="{{asset('css/fade(voorstel2).css')}}" rel="stylesheet">

</head>
<header style="position:fixed;  z-index: 100; width: 100%">
    <nav id="hoofd"
         style="background: url('https://cdn.discordapp.com/attachments/490049803816796160/651708551353335809/galaxy.png')">
        <div style="padding-bottom: 1%;padding-top:1%;text-align: center">
            <a style="text-decoration: underline;color: whitesmoke; font-size: 24px; padding-right: 70%"
               href="/"><img
                    src="https://nineplanets.org/wp-content/uploads/2019/09/earth.png"
                    alt="planet" height="5%"
                    width="5%">   Landstede </a>
            <a style="text-decoration: underline; color: whitesmoke; font-size: 24px; padding-left: 70%"
               href="/about">Info   <img
                    src="https://carlisletheacarlisletheatre.org/images/moon-png-3.png"
                    alt="planet" height="3%"
                    width="3%"></a>
        </div>
    </nav>
</header>

<body>

<div id="container" class="desaus">

    <?php

    use App\Http\Controllers\carouselController;use App\Http\Controllers\databaseController;

    $db = new databaseController();
    $db->checkSlides();
    //haal de lanparty datum op

    ?>
    <?php
    $carousel = new carouselController();
    $carousel->carousel();
    for ($x = 0; $x <= 50; $x++) {
    ?><h1>XD</h1><?php
    }

    ?>
    <?php $db->lanTijd();?>
    <div class="container" style="text-align: center; ">
        <div class="container" style="width: 900px;">
            <div class="row justify-content-center">
                <p id="timer" style="color:mediumpurple;
                    font-style: italic;
                    font: 72px 'Orbitron', sans-serif;

                       "></p>
            </div>
        </div>
        <?php
        if ($db->datum() != NULL) {
        ?><h3 style="color:mediumpurple">{{$db->datum()}}</h3>
        <?php
        } else {
            echo "Er is nog geen datum gesteld";
        }
        ?>
        <form action="/register">
            {{csrf_field()}}
            <button type="submit" style="background-color: #f5fcfc; color:mediumpurple" class="btn btn-lg">
                Schrijf je nu in!
            </button>
        </form>
    </div>
</div>

</body>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js">
    var nystories = document.querySelector("p").offsetTop;

    window.onscroll = function () {
        if (window.pageYOffset > 0) {
            var opac = (window.pageYOffset / nystories);
            console.log(opac);
            document.body.style.background = "linear-gradient(rgba(255, 255, 255, " + opac + "), rgba(255, 255, 255, " + opac + ")), url(https://s3-us-west-2.amazonaws.com/s.cdpn.io/4273/times-square-perspective.jpg) no-repeat";
        }
    }

</script>
<script type="text/javascript">


</script>
</html>






