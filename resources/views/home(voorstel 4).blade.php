@extends('layouts.app')

@section('qrScripts')
    <script src="{{ asset('js/app.js') }}" defer></script>
@endsection

@section("content")
    <?php
    session_start();
    use App\Http\Controllers\carouselController;use App\Http\Controllers\databaseController;

    $db = new databaseController();
    $db->checkSlides();
    //haal de lanparty datum op

    ?>
    <div id="countdownKnop">
        <div id="notif" class="alert alert-warning alert-dismissible fade show" role="alert">
            <strong>LET OP!</strong> <a style="color:black;">Je kan met de pijltjes toetsen door de secties heen
                bladeren.</a>
            <button onclick="setSesh()" value="ontslagen" type="button" class="close" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>

        <div class="container" style="padding-top: 7%; bottom:30%; text-align: center;">
            <hr class="lightbar" style="border: 4px solid white; text-align:center;">
            <br>
            <div>{{$db->lanTijd()}}</div>
            <div class="container" style="width: 900px;">
                <div class="row justify-content-center">
                    <p id="timer" style="font-size:72px;color:white"></p>
                </div>
            </div>
            <?php
            if ($db->datum() != NULL) {
            ?><h3 style="margin-right: 10px; color:white;">{{$db->datum()}}</h3>
            <?php
            } else {
                echo "Er is nog geen datum gesteld";
            }
            ?>
            <form action="/register">
                {{csrf_field()}}
                <button type="submit" style="  margin-right: 10px;background-color: #f5fcfc; color:black
                " class="btn btn-lg">
                    Inschrijven
                </button>
            </form>
            <br>
            <hr class="lightbar" style="border: 5px solid white;width: 80%;text-align:center;">
            <br>
        </div>

    </div>

    <div id="divB" class="block">
        <div style="margin-top:50%;" id="container">
            <div style="text-align:center">
                <div id="dp" style="z-index: 1; background: black;">
                    <h1 style="color:white;">Fotoalbum carousel</h1>
                    <?php
                    $carousel = new carouselController();
                    $carousel->carousel();
                    ?>
                </div>
            </div>
        </div>
    </div>
    <div style="margin-top: 50%" id="divC" class="block">
        <?php
        for ($x = 0; $x <= 10; $x++) {
            echo "zonder saus <br>";
        }
        ?>
    </div>


    <div id="sidenav" class="sidenav" style=" display: none ;opacity: 0; margin-top: 10%">
        <a style="color: whitesmoke; font-size: 24px;" href="/">
            <img src="https://nineplanets.org/wp-content/uploads/2019/09/earth.png" alt="planet" height="17%"
                 width="17%">
            Landstede
        </a>

        <a style="margin-left:2.5%;color: whitesmoke; font-size: 24px;" href="/">
            <img src="https://carlisletheacarlisletheatre.org/images/moon-png-3.png" alt="planet" height="10%"
                 width="10%">
            Info
        </a>
    </div>
    <script>
        function setSesh() {
            document.getElementById("notif").style.display = "none";
            sessionStorage.setItem("notifn", "off");
        }

        if (sessionStorage.getItem("notifn") === "off") {
            document.getElementById("notif").style.display = "none";
        } else {
            document.getElementById("notif").style.display = "block";
        }
    </script>

    <script>
        document.onscroll = function () {
            document.getElementById("sidenav").style.opacity = window.pageYOffset / document.getElementById('hoofd').scrollHeight;
            document.getElementById("sidenav").style.display = "inline";
        };
    </script>

    <script>
        let startPunt = 0;

        window.onload = function () {
            setTimeout(function () {
                scrollTo(0, -1);
                startPunt = 0;
            }, 0);
        };

        for (x = 0; x < 10; x++) {

        }

        document.onkeydown = function (event) {
            switch (event.key) {
                case "ArrowUp":
                    document.getElementsByClassName("block")[startPunt - 1].scrollIntoView({
                        behavior: "smooth",
                        block: "center"
                    });
                    startPunt--;
                    break;
                case "ArrowDown":
                    document.getElementsByClassName("block")[startPunt + 1].scrollIntoView({
                        behavior: "smooth",
                        block: "center"
                    });
                    startPunt++;
                    break;
                /// WATCH OUT ADMIN DANGEROUS STAY OUT
                case "Dead":

                    break;
            }
        }
        ;


    </script>

@endsection





