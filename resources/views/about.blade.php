@extends('layouts.app')

@section('qrScripts')
    <script src="{{ asset('js/app.js') }}" defer></script>
    <link href="{{asset('css/app.css')}}" rel="stylesheet">
@endsection

@section('content')
    <?php
    use App\Http\Controllers\databaseController;use Illuminate\Support\Facades\DB;

    $temp = new databaseController();
    $leraren = $temp->verkrijgLeraren();

    try {
        $admindingen = DB::table('admin_changes')->get();
        $tldr = $admindingen[0]->textTLDR;
        $titel = $admindingen[0]->textTitel;
        $text = $admindingen[0]->textText;
    } catch (Exception $e) {

    }?>
    <div style="padding-top:14%; ;font-size :24px; ">

        <div class="container" style=" -webkit-box-shadow: 0 1px 5px 0 rgba(0,0,0,0.75);
-moz-box-shadow: 0px 1px 5px 0px rgba(0,0,0,0.75);
box-shadow: 0px 1px 5px 0px rgba(0,0,0,0.75);">
            <div class="row">
                <div class="col"></div>
                <div class="col">Maandag</div>
                <div class="col">Dinsdag</div>
                <div class="col">Woensdag</div>
                <div class="col">Donderdag</div>
                <div class="col">Vrijdag</div>
                <div class="w-100"></div>
                <?php  foreach ($leraren as $leraar) {
                $lnaam = $leraar->leraar_naam;
                $benschop = array(
                    $beschop[0] = $leraar->ma,
                    $beschopd[1] = $leraar->di,
                    $beschopw[2] = $leraar->wo,
                    $beschop[3] = $leraar->do,
                    $beschop[4] = $leraar->vr,
                );
                $id = $leraar->leraar_id;
                ?>
                <div class="col">{{$lnaam}}</div>
                <div class="col"><?php if ($benschop[0] == 1) {
                        echo "X";
                    } else {
                        echo " ";
                    }?></div>
                <div class="col"><?php if ($benschop[1] == 1) {
                        echo "X";
                    } else {
                        echo " ";
                    }?></div>
                <div class="col"><?php if ($benschop[2] == 1) {
                        echo "X";
                    } else {
                        echo " ";
                    }?></div>
                <div class="col"><?php if ($benschop[3] == 1) {
                        echo "X";
                    } else {
                        echo " ";
                    }?></div>
                <div class="col"><?php if ($benschop[4] == 1) {
                        echo "X";
                    } else {
                        echo " ";
                    }?></div>
                <div class="w-100"></div>
                <?php } ?>
            </div>
        </div>
        <br>
        <div style="text-align: center">
            <h1>"TL;DR"</h1>
            <h2>{{$tldr}}</h2>
        </div>

        <div>
            <div style="padding: 8%; padding-bottom: 0;padding-top: 3%;">
                <h1>{{$titel}}</h1>
                <h3 class="form-control" style="white-space: pre-wrap; height:auto; font-size: 24px">{{$text}}</h3>
            </div>
        </div>
    </div>
@endsection
