@extends("layouts.app")

@section('qrScripts')
    <script src="https://unpkg.com/vue@2.6.10/dist/vue.min.js"></script>
    <script src="https://unpkg.com/vue-qrcode-reader@2.0.3/dist/vue-qrcode-reader.browser.js"></script>
    <link rel="stylesheet" href="https://unpkg.com/vue-qrcode-reader@2.0.3/dist/vue-qrcode-reader.css">
    <link href="{{asset('css/app.css')}}" rel="stylesheet">

@endsection

@section('content')
    <div id="app" style="padding-top: 14%;">
        <div class="container">
            <div class="justify-content-center">
                <h1 style="text-align: center">scan da tickat</h1><br>
            </div>
        </div>
        <?php
        if (isset($_GET['zetGekocht'])) {
            Illuminate\Support\Facades\DB::table('users')
                ->where('user_qr', $_GET['qrqr'])
                ->update(['gekocht' => 1]);

        }

        //Qr code checker
        if (isset($_GET['key'])) {
            $qr_codes = Illuminate\Support\Facades\DB::select('SELECT user_qr FROM users');
            $qr_codes = json_decode(json_encode($qr_codes), True);

            try {
                $fullQR = "?key=" . $_GET['key'] . "&food=" . $_GET['food'];
                $_SESSION['fullQR'] = "?key=" . $_GET['key'] . "&food=" . $_GET['food'];
                $result = Illuminate\Support\Facades\DB::select("SELECT gekocht FROM `users` WHERE user_qr='$fullQR '");
                $result = json_decode(json_encode($result), True);

                if ($result == NULL) {
                    echo("<h1>Je qr code is ongeldig </h1>");
                }

                foreach ($result as $urResult) {
                    if ($urResult['gekocht'] == 1) {
                        $hasbought = (boolval($urResult['gekocht']));
                        echo "<h1 style='text-align: center'>Bro je mag naar binnen. Vanavond hebben we voor jouw een " . $_GET['food'] . "!</h1><br>";
                    } elseif ($urResult['gekocht'] == 0) {
                        echo '<form method="get"><input type="submit" name="zetGekocht" value="Student heeft betaalt"><input type="hidden" name="qrqr" value="' . $fullQR . '"></form>';
                        echo "<h1 style='text-align: center'>Nog niet betaald</h1>";
                    }
                }
            } catch (Exception $e) {
                echo "<h1>Je qr code is nep<h1>";
            }

        }
        ?>
        <qrcode-stream @decode="onDecode" @init="onInit">
            <img style="text-align: center;width: 100%; height: 100%"
                 src="https://3.bp.blogspot.com/-EGqUmgHuOF4/WlHhgrnraLI/AAAAAAAAPt4/h2yMF6w0GSQXNGNjN5AWZGuRVyYjYLIAgCLcBGAs/s1600/delete.png"
                 alt="">
        </qrcode-stream>
    </div>



    <script>
        new Vue({

            el: '#app',

            data() {
                return {
                    decodedContent: '',
                    errorMessage: 'zappie'
                }
            },

            methods: {
                onDecode(content) {
                    this.decodedContent = content;

                    if (content === '') {
                        '<h1>??????????????????</h1>'
                    }
                    // else if (containsLink === true) { }
                    else if (content.includes('.') === false) {
                        window.location.href = content
                    }


                    // } else {
                    //     var containsLinkCO = content.indexOf('https://');
                    //     var containsWWWCO = content.indexOf('wwww.');
                    //
                    // }

                },

                onInit(promise) {
                    promise.then(() => {
                        console.log('Successfully initilized! Ready for scanning now!')
                    })
                        .catch(error => {
                            if (error.name === 'NotAllowedError') {
                                this.errorMessage = 'Hey! I need access to your camera'
                            } else if (error.name === 'NotFoundError') {
                                this.errorMessage = 'Do you even have a camera on your device?'
                            } else if (error.name === 'NotSupportedError') {
                                this.errorMessage = 'Seems like this page is served in non-secure context (HTTPS, localhost or file://)'
                            } else if (error.name === 'NotReadableError') {
                                this.errorMessage = 'Couldn\'t access your camera. Is it already in use?'
                            } else if (error.name === 'OverconstrainedError') {
                                this.errorMessage = 'Constraints don\'t match any installed camera. Did you asked for the front camera although there is none?'
                            } else {
                                this.errorMessage = 'UNKNOWN ERROR: ' + error.message
                            }
                        })
                }
            }
        })
        ;
    </script>
@endsection
