<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>


    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://fonts.googleapis.com/css?family=Orbitron&display=swap" rel="stylesheet">


    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('title','Lanparty')</title>

    <!-- Scripts -->

    <script src="{{ asset('js/app.js') }}" defer></script>


    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{asset('css/app.css')}}" rel="stylesheet">
    <link href="{{asset('css/fade(voorstel3).css')}}" rel="stylesheet">

</head>
<header>
    <nav id="hoofd"
         style="background: url('https://i.ytimg.com/vi/1iIXx55Orwg/maxresdefault.jpg')">
        <div style="padding-bottom: 1%;padding-top:1%;text-align: center">
            <a style="text-decoration: underline;color: whitesmoke; font-size: 24px; padding-right: 70%"
               href="/"><img
                    src="https://nineplanets.org/wp-content/uploads/2019/09/earth.png"
                    alt="planet" height="5%"
                    width="5%">   Landstede </a>
            <a style="text-decoration: underline; color: whitesmoke; font-size: 24px; padding-left: 70%"
               href="/about">Info   <img
                    src="https://carlisletheacarlisletheatre.org/images/moon-png-3.png"
                    alt="planet" height="3%"
                    width="3%"></a>
        </div>
    </nav>
</header>
<?php

use App\Http\Controllers\carouselController;use App\Http\Controllers\databaseController;

$db = new databaseController();
$db->checkSlides();
//haal de lanparty datum op

?>
<body>
<div id="sidenav" class="sidenav" style="margin-top: 10%">

</div>
<div style="" id="container" class="desaus">
    <div style="text-align:center">
        <div class="container" style="text-align: center; ">
            <hr class="lightbar" style="border: 4px solid white; text-align:center;">
            <br>
            <?php $db->lanTijd();?>
            <div class="container" style="width: 900px;">
                <div class="row justify-content-center">
                    <p id="timer" style="font-size:72px"></p>
                </div>
            </div>
            <?php
            if ($db->datum() != NULL) {
            ?><h3 style="margin-right: 10px">{{$db->datum()}}</h3>
            <?php
            } else {
                echo "Er is nog geen datum gesteld";
            }
            ?>
            <form action="/register">
                {{csrf_field()}}
                <button type="submit" style="  margin-right: 10px;background-color: #f5fcfc; color:black
                " class="btn btn-lg">
                    Inschrijven
                </button>
            </form>
            <br>
            <hr class="lightbar" style="border: 5px solid white;width: 80%;text-align:center;">
            <br>
        </div>

        <div style=" background: rgba(255,255,255,0.5); margin-top: 20%">
            <?php
            $carousel = new carouselController();
            $carousel->carousel();
            for ($x = 0; $x <= 50; $x++) {
            ?>
            <h1>hi</h1>
            <?php
            }

            ?>
        </div>
    </div>
</div>

</body>


<script>

    document.onscroll = function () {
        console.log(document.getElementById("hoofd").scrollTop);

        if (window.pageYOffset < 170) {
            document.getElementById("sidenav").innerHTML = "";
        }

        if (window.pageYOffset > 170) {

            document.getElementById("sidenav").innerHTML =
                "  <div >  <a style=\"color: whitesmoke; font-size: 24px;\" href=\"/\">\n" +
                "        <img src=\"https://nineplanets.org/wp-content/uploads/2019/09/earth.png\" alt=\"planet\" height=\"17%\" width=\"17%\">\n" +
                "        Landstede\n" +
                "    </a>\n" +
                "\n" +
                "    <a style=\"margin-left:2.5%;color: whitesmoke; font-size: 24px;\" href=\"/\">\n" +
                "        <img src=\"https://carlisletheacarlisletheatre.org/images/moon-png-3.png\" alt=\"planet\" height=\"10%\" width=\"10%\">\n" +
                "        Info\n" +
                "    </a>\n" +
                "</div>\n"
        }
    }


</script>

<
/html>






