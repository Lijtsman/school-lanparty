<?php

use App\Mail\hasBought;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Route;


Auth::routes();


Route::get('/purchase', 'userController@index');

Route::get('/about', function () {
    return view('about');
});

Route::get('/buy', function () {
    return view('buy');
})->middleware('auth');

Route::get('/', 'HomeController@index')->name('home');
Route::get('/home', 'HomeController@index')->name('home');

Route::get('/emailtest', function () {
    return view('hasbought');
});


Route::any('/admin', function () {
    return view('security');
})->middleware('auth');

Route::post('/adminPanel', function () {
    return view('changeadminchange');
});

Route::any('/admininfo', function () {
    return view('admininfo');
});

Route::any('/qrscanner', function () {
    return view('qrscanner');
});

Route::get('/qrcode', function () {
    Mail::to('joellijtsman@hotmail.com')->send(new hasBought());

//    $qrcode = QrCode::size(300)->generate('test123');
//    mail('joel.lijtsman@mcmain.nl', 'Je bent een zapiie', 'Bro je hebt zojuist een fucking ui gegeten lmao<br>' . $qrcode . '');
    echo 'klaar!';

    //Auth::user()->user_qr
});






