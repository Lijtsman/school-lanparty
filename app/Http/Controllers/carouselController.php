<?php

namespace App\Http\Controllers;

class carouselController extends Controller

{
    private $db;

    public function __construct()
    {
        $this->db = new databaseController();
    }

    public function carousel()
    {
        $adminPlaatjes = $this->db->verkrijgPlaatjes()
        ?>
        <div id="carousel" style="padding: 2%">
            <div id="carouselExampleIndicators" class="carousel slide lightbar"
                 data-ride="carousel">
                <ol style="background-color:rgba(0,0,0,.2);" class="carousel-indicators">
                    <?php
                    $first = true;
                    foreach ($adminPlaatjes as $adminPlaatje) {
                        if ($first = true) {
                            echo '<li data-target="#carouselExampleIndicators" data-slide-to=' . $adminPlaatje->image_id . ' class="active"></li>';
                            $first = false;
                        } else {
                            echo '<li data-target="#carouselExampleIndicators" data-slide-to=' . $adminPlaatje->image_id . '></li>';
                        }
                    }
                    ?>
                </ol>
                <div class="carousel-inner">
                    <?php
                    $first = true;
                    foreach ($adminPlaatjes as $adminPlaatje) {
                        if ($first == true) {
                            echo '<div class="carousel-item active"><img height="640px" width="640px" class="d-block w-100" src=' . $adminPlaatje->slide_image . ' alt="First slide">';
                            $first = false;
                            //Zet de naam en beschrijving van het plaatje als die beschikbaar zijn
                            if (isset($adminPlaatje->img_desc) or isset($adminPlaatje->img_name)) {
                                echo '<div class="carousel-caption d-none d-md-block" style="background-color:rgba(0,0,0,.3);">';
                                if (isset($adminPlaatje->img_name)) {
                                    echo '<h5>' . $adminPlaatje->img_name . '</h5>';
                                }
                                if (isset($adminPlaatje->img_desc)) {
                                    echo '<p> ' . $adminPlaatje->img_desc . '</p>';
                                }
                                echo '</div>';
                            }
                            echo '</div>';
                        } else {
                            echo '<div class="carousel-item" ><img height = "640px" width = "640px" class="d-block w-100" src = ' . $adminPlaatje->slide_image . ' alt = "not the first slide" >';
                            if (isset($adminPlaatje->img_desc) or isset($adminPlaatje->img_name)) {
                                echo '<div class="carousel-caption d-none d-md-block" style="background-color:rgba(0,0,0,.3);">';
                                if (isset($adminPlaatje->img_name)) {
                                    echo '<h5>' . $adminPlaatje->img_name . '</h5>';
                                }
                                if (isset($adminPlaatje->img_desc)) {
                                    echo '<p> ' . $adminPlaatje->img_desc . '</p>';
                                }
                                echo '</div>';
                            }
                            echo '</div>';
                        }
                    }
                    ?>
                </div>
                <a style="width: 50%;" class="carousel-control-prev"
                   href="#carouselExampleIndicators" role="button" data-slide="prev">
                    <span class="sr-only">Previous</span>
                </a>
                <a style="width: 50%;" class="carousel-control-next"
                   href="#carouselExampleIndicators" role="button" data-slide="next">
                    <span class="sr-only">Next</span>
                </a>
            </div>
        </div>
        <script>
            $(' . carousel').carousel();
        </script>
        <?php
    }


}

