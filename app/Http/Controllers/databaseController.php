<?php


namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;

class databaseController extends Controller
{

    public function verkrijgLeraren()
    {
        return DB::table('leraar_info')->get();
    }

    public function verkrijgUsers($id = null)
    {
        if (isset($id)) {
            return DB::table('users')->where("id", $id)->get();
        } else {
            return DB::table('users')->get();
        }
    }

    public function verkrijgAdmin()
    {
        return DB::table('admin_changes')->get();

    }

    public function verkrijgPlaatjes()
    {
        return DB::table('slide_images')->get();
    }

    public function checkSlides()
    {//als de default image de eerste value uit de database is en er komen daarna andere verwijder dan het plaatje en herlaad de pagina
        if (count($this->verkrijgPlaatjes()) > 1 and $this->verkrijgPlaatjes()[0]->slide_image == "https://3mxwfc45nzaf2hj9w92hd04y-wpengine.netdna-ssl.com/wp-content/uploads/2015/03/inside-page-style-blank-3.jpg") {
            DB::table('slide_images')->where("slide_image", "=", "https://3mxwfc45nzaf2hj9w92hd04y-wpengine.netdna-ssl.com/wp-content/uploads/2015/03/inside-page-style-blank-3.jpg")->delete();
            echo "<script>location.reload();</script>";
        }
    }

    public function datum()
    {
        foreach ($this->verkrijgAdmin() as $adminding) {
            $datum = $adminding->lanparty_datum;
        }

        return $datum;
    }

    public function lanTijd()
    {
        $datum = $this->datum();

        if ($datum !== NULL) {
            echo '<label  id="deDatum" class="' . $datum . '"></label><script>var countDownDate = new Date(document.getElementById("deDatum").className).getTime();var x = setInterval(function() {var now = new Date().getTime();var distance = countDownDate - now;var days = Math.floor(distance / (1000 * 60 * 60 * 24));var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));var seconds = Math.floor((distance % (1000 * 60)) / 1000);document.getElementById("timer").innerHTML = days + "d " + hours + "h "+ minutes + "m " + seconds + "s ";if (distance < 0) {clearInterval(x);document.getElementById("timer").innerHTML = "De lanparty is nu";}}, 1000);</script>';
        }

    }

    public function registerStatus()
    {
        foreach ($this->verkrijgAdmin() as $adminTory) {
            $rState = $adminTory->rState;
        }
        return ($rState);
    }

    public function registerUpdate($stateNum)
    {
        DB::table('admin_changes')->update(['rState' => $stateNum]);
    }

    public function hellemaalStom()
    {

    }

//    public function stuurLeraar($naam. )
//    {
//        DB::table('slide_images')->insert(['image_id' => NULL, 'slide_image' => $_POST['newImgLink'], 'img_name' => $_POST['img_name'], 'img_desc' => $_POST['img_desc']]);
//    }
}

