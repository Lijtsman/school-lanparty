<?php

namespace App\Http\Controllers;

use App\Mail\hasBought;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Mail;

class QrController extends Controller
{
    public function getQr($food)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvlowxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $charactersLength; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        $randomString = "?key=$randomString&food=$food";
        return $randomString;
    }

    public function buyTicket($email)
    {
        Mail::to($email)->send(new hasBought());

        File::delete('../public/images/qrcodes/qrcode' . Auth::user()->id . '.png');
    }

}

